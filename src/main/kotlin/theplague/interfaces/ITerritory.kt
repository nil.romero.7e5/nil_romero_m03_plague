package theplague.interfaces

interface ITerritory {
    val icons : MutableList<Iconizable>

    /**
     * List of icons of the current territory (4 max)
     */
    fun iconList() : List<Iconizable>

    fun addIcon(icon: Iconizable)

    fun removeIcon(icon: Iconizable)
}