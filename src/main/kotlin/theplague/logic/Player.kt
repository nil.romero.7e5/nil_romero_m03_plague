package theplague.logic

import theplague.interfaces.IPlayer
import theplague.interfaces.Iconizable
import theplague.interfaces.Position
import theplague.logic.items.OnFoot


class Player(
    override var turns: Int,
    override var livesLeft: Int,
    override var currentWeapon: Iconizable,
) : IPlayer {

    override var position : Position = Position(3,3);

    override val icon: String
        get() = "\uD83D\uDEB6"

    override var currentVehicle: Iconizable = OnFoot();
}
