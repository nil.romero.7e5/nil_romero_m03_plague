package theplague.logic.colony

import theplague.interfaces.Position
import theplague.logic.colony.Colony

data class Colonization (val colony: Colony, val position: Position ) {

}