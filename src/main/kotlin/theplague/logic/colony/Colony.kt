package theplague.logic.colony

import theplague.interfaces.Iconizable
import theplague.interfaces.Position
import theplague.logic.items.Broom
import theplague.logic.items.Hand
import theplague.logic.items.Sword
import theplague.logic.items.Weapon
import kotlin.math.abs

sealed class Colony : Iconizable {

    var size : Int = 1;
    abstract val primaryIcon : String
    abstract val membersToLoseByHand : Int
    abstract val membersToLoseBySword : Int
    abstract val membersToLoseByBroom : Int

    abstract fun willReproduce() : Boolean

    fun reproduce(){
        if (size < 4 ) {
            size++
        };
    }

    fun needsToExpand() : Boolean{
        return size == 4;
    }

    fun attacked(weapon: Weapon){
        size -= when(weapon){
            is Hand ->{
                if (size - membersToLoseByHand < 0) {
                    1
                } else {
                    membersToLoseByHand;
                }
            }
            is Broom ->{
                membersToLoseByBroom
            }
            is Sword ->{
                membersToLoseBySword;
            }
        }
    }

    fun expand(position: Position, radius : Int) : Colonization {

        var newPosition : Position
        do {
            newPosition = Position(position.x + (-radius..radius).random(), position.y + (-abs(radius)..radius).random())
            println(newPosition)
        } while (newPosition == position)
        return Colonization(Ant(), newPosition)
    }




}