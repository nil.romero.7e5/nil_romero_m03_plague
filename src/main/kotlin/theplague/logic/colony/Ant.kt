package theplague.logic.colony

class Ant : Colony() {
    override val icon: String
        get() = primaryIcon.repeat(size)

    override val primaryIcon : String
        get() = "\uD83D\uDC1C"

    override val membersToLoseByHand: Int
        get() = 2
    override val membersToLoseByBroom: Int
        get() = size
    override val membersToLoseBySword: Int
        get() = 1

    override fun willReproduce() : Boolean{
        return (1..100).random() <= 30;
    }
}