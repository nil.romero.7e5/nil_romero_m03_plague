package theplague.logic.colony

class Dragon : Colony() {
    override val icon: String
        get() = primaryIcon.repeat(size)

    override val primaryIcon: String
        get() = "\uD83D\uDC09"

    override val membersToLoseByHand: Int
        get() = 0
    override val membersToLoseByBroom: Int
        get() = 0
    override val membersToLoseBySword: Int
        get() = 1

    var turns : Int = 0;

    override fun willReproduce() : Boolean{
        val result = turns == 5
        turns++
        return result
    }

}