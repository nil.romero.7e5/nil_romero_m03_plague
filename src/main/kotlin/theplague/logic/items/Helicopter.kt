package theplague.logic.items

import theplague.logic.items.Vehicle

class Helicopter : Vehicle() {
    override var turnLeft: Int = 5
    override var hasTurnsLimit: Boolean = true

    override val icon: String
        get() = "\uD83D\uDE81"
}