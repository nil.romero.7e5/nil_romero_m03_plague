package theplague.logic.items

import theplague.logic.items.Weapon

class Broom : Weapon() {
    override var turnLeft: Int = -1
    override var hasTurnsLimit: Boolean = true

    override val icon: String
        get() = "\uD83E\uDDF9"
}