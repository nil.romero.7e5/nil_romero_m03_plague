package theplague.logic.items

import theplague.logic.items.Weapon

class Sword : Weapon() {
    override var turnLeft: Int = -1
    override var hasTurnsLimit: Boolean = true

    override val icon: String
        get() = "\uD83D\uDDE1"
}