package theplague.logic.items

import theplague.logic.items.Vehicle

class OnFoot : Vehicle() {
    override var turnLeft: Int = -1
    override var hasTurnsLimit: Boolean = true
    override val icon: String
        get() = "\uD83D\uDEB6"
}