package theplague.logic

import theplague.interfaces.*
import theplague.logic.colony.Ant
import theplague.logic.colony.Colonization
import theplague.logic.colony.Colony
import theplague.logic.colony.Dragon
import theplague.logic.items.*
import kotlin.math.abs
import kotlin.math.max

class DefaultIcon(override val icon: String) : Iconizable { }

class World(
    override val width: Int,
    override val height: Int,
    override val territories: List<List<ITerritory>>,
    override val player: IPlayer,
    override val colonizations: MutableList<Colonization> = mutableListOf()
): IWorld {
    override fun randomPosition(): Position {
        val x = (territories.indices).random()
        val y = (territories[x].indices).random()
        return Position(x,y)
    }


    override fun nextTurn() {
        reproduceAndExpand()
        generateNewColonies()
        generateNewItems()
        spendWeaponTurns()
        cleanMap()
        println("next turn started")
        player.turns++
    }

    private fun cleanMap() {
        territories.forEach {
            it.forEach { territory ->
                territory.icons.forEach { icon ->
                    if (icon is Colony && icon.size == 0) {
                        territory.icons.remove(icon)
                    }
                }
            }
        }
    }

    private fun spendWeaponTurns() {
        if ((player.currentVehicle as Vehicle).turnLeft-- == 0) {
            player.currentVehicle = OnFoot()
            println("exhausted vehicle turns left")
        }
    }

    override fun reproduceAndExpand(){
        colonizations.forEach {
            var newColonization : Colonization
            if (it.colony.needsToExpand()){
                if (it.colony is Ant) {
                    do {
                        newColonization = it.colony.expand(it.position, 1);
                    } while (!correctPosition(newColonization.position))
                    if (!territoryContainsColony(newColonization.position)){
                        territories[newColonization.position.x][newColonization.position.y].addIcon(newColonization.colony)
                    }
                    territories[newColonization.position.x][newColonization.position.y].addIcon(newColonization.colony)
                    colonizations.add(newColonization)
                    player.livesLeft--
                } else {
                    val position = randomPosition()
                    territories[position.x][position.y].addIcon(Dragon())
                    colonizations.add(Colonization(Dragon(), position))
                    player.livesLeft--
                }

            } else {
                if (it.colony.willReproduce()){
                  it.colony.reproduce()
                }
            }
        }

    }

    private fun territoryContainsColony(position: Position): Boolean {
        //TODO
        return false
    }

    private fun correctPosition(position: Position): Boolean {
        return position.x in 0..7 && position.y in 0..7
    }

    override fun gameFinished(): Boolean {
        println("game finish checked")
        return player.livesLeft == 0;
    }

    override fun canMove(position: Position): Boolean {
        println("can move checked")
        var canMove = false;

        val deltaMoveY = abs(position.y - player.position.y);
        val deltaMoveX = abs(position.x - player.position.x);
        val dist = max(deltaMoveY, deltaMoveX);

        when(player.currentVehicle){
            is OnFoot ->{
                canMove = dist < 2;
            }
            is Bicycle ->{
                canMove = dist < 5;
            }
            is Helicopter ->{
                canMove = true;
            }
        }
        return canMove;
    }

    override fun moveTo(position: Position) {

        println("player move")

        val lastTerr : ITerritory = territories[player.position.y][player.position.x];
        val targetTerr : ITerritory = territories[position.y][position.x];

        lastTerr.removeIcon(player);
        targetTerr.addIcon(player);

        player.position = position

    }

    override fun exterminate() {

        val cell = territories[player.position.y][player.position.x];

        cell.icons.forEach { colony ->
            if (colony is Colony) {
                when (colony) {
                    is Ant -> colony.attacked(player.currentWeapon as Weapon)
                    is Dragon -> colony.attacked(player.currentWeapon as Weapon)
                }
            }
        }
    }

    override fun takeableItem(): Iconizable? {
        println("takeable item checked")

        val cell = territories[player.position.y][player.position.x];

        cell.icons.forEach {
            if(it is Item){
                return it;
            }
        }
        return null
    }

    override fun takeItem(item : Item) {
        if(item is Vehicle){
            player.currentVehicle = item;
        }else if (item is Weapon) {
            player.currentWeapon = item;

        }

        val cell = territories[player.position.y][player.position.x]
        cell.removeIcon(item);
        println("item taken")
    }

    override fun generateNewItems() {
        var newItem : Item? = null
        with(territories) {
            when ((1..100).random()) {
                in 31..55 -> {
                    newItem = Bicycle()
                }
                in 56..65 -> {
                    newItem = Helicopter()
                }
                in 66..90 -> {
                    newItem = Broom()
                }
                in 91..100 -> {
                    newItem = Sword()
                }
            }
            if (newItem!=null) {
                random().random().addIcon(newItem!!)
                println("item generated")
            }
        }

    }

    override fun generateNewColonies() {
        var newColony : Colony? = null
        when ((1..100).random()) {
            in 1..30 -> {
                newColony = Ant()
            }
            in 31..40 -> {
                newColony = Dragon()
            }
        }
        if (newColony!=null) {
            val position = randomPosition()
            territories[position.x][position.y].addIcon(newColony)
            colonizations.add(Colonization(newColony,position))
            println("colony generated")
        }
    }
}